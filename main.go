package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

// DB is ...
type DB struct {
	limit *sql.DB
}

// LimitSet is ...
type LimitSet struct {
	ID               string          `json:"id"`
	LowerWarning     sql.NullFloat64 `json:"lowerWarning"`
	LowerAlarm       sql.NullFloat64 `json:"lowerAlarm"`
	UpperWarning     sql.NullFloat64 `json:"upperWarning"`
	UpperAlarm       sql.NullFloat64 `json:"upperAlarm"`
	LowerWarningInfo sql.NullString  `json:"LowerWarningInfo"`
	LowerAlarmInfo   sql.NullString  `json:"LowerAlarmInfo"`
	UpperWarningInfo sql.NullString  `json:"UpperWarningInfo"`
	UpperAlarmInfo   sql.NullString  `json:"UpperAlarmInfo"`
}

func (db *DB) readLimits() []LimitSet {

	// Execute the query
	results, err := db.limit.Query("SELECT id, lower_warning, lower_alarm, upper_warning, upper_alarm, lower_warning_info, lower_alarm_info, upper_warning_info, upper_alarm_info FROM limitset")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	limitSets := []LimitSet{}

	for results.Next() {
		var limitset LimitSet
		// for each row, scan the result into our tag composite object
		err = results.Scan(&limitset.ID, &limitset.LowerWarning, &limitset.LowerAlarm, &limitset.UpperWarning, &limitset.UpperAlarm, &limitset.LowerWarningInfo, &limitset.LowerAlarmInfo, &limitset.UpperWarningInfo, &limitset.UpperAlarmInfo)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		limitSets = append(limitSets, limitset)
	}
	return limitSets
}

func (db *DB) writeLimits(limitSets []LimitSet) {

	var skippedLimits []int

	for _, limitset := range limitSets {

		sqlStatement := `INSERT INTO limit_object (id, limitset_id, type_id, value, predicate, unit_id, info, dead_time) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`

		if limitset.LowerWarning.Valid {
			limitUUID := uuid.New()
			_, err := db.limit.Exec(sqlStatement, limitUUID, limitset.ID, "71cefd50-c9dd-4992-bde2-e2ffd3ccae82", limitset.LowerWarning, "lt", nil, limitset.LowerWarningInfo, nil)
			if err != nil {
				panic(err)
			}
		}

		if limitset.UpperWarning.Valid {
			limitUUID := uuid.New()
			_, err := db.limit.Exec(sqlStatement, limitUUID, limitset.ID, "71cefd50-c9dd-4992-bde2-e2ffd3ccae82", limitset.UpperWarning, "gt", nil, limitset.UpperWarningInfo, nil)
			if err != nil {
				panic(err)
			}
		}

		if limitset.LowerAlarm.Valid {
			limitUUID := uuid.New()
			_, err := db.limit.Exec(sqlStatement, limitUUID, limitset.ID, "44caff7a-9186-4af5-8674-3ec3f2a0d4d8", limitset.LowerAlarm, "lt", nil, limitset.LowerAlarmInfo, nil)
			if err != nil {
				panic(err)
			}
		}

		if limitset.UpperAlarm.Valid {
			limitUUID := uuid.New()
			_, err := db.limit.Exec(sqlStatement, limitUUID, limitset.ID, "44caff7a-9186-4af5-8674-3ec3f2a0d4d8", limitset.UpperAlarm, "gt", nil, limitset.UpperAlarmInfo, nil)
			if err != nil {
				panic(err)
			}
		}

	}
	log.Printf("Done! Skipped limits: %v", skippedLimits)
}

func (db *DB) printLimits(limitSets []LimitSet) {
	for _, limitset := range limitSets {
		log.Printf("Limit ID: %d, Target Value: %f, SignalID: %s", limitset.ID, limitset.LowerWarning.Float64, limitset.LowerWarningInfo)
	}
}

func printLimit(limitset LimitSet) {
	var sLowerAlarm, sLowerWarning, sUpperAlarm, sUpperWarning string

	if limitset.LowerAlarm.Valid {
		sLowerAlarm = fmt.Sprintf("LowerAlarm: %f", limitset.LowerAlarm.Float64)
	} else {
		sLowerAlarm = fmt.Sprintf("LowerAlarm: NULL")
	}
	if limitset.LowerWarning.Valid {
		sLowerWarning = fmt.Sprintf("LowerWarning: %f", limitset.LowerWarning.Float64)
	} else {
		sLowerWarning = fmt.Sprintf("LowerWarning: NULL")
	}
	if limitset.UpperAlarm.Valid {
		sUpperAlarm = fmt.Sprintf("UpperAlarm: %f", limitset.UpperAlarm.Float64)
	} else {
		sUpperAlarm = fmt.Sprintf("UpperAlarm: NULL")
	}
	if limitset.UpperWarning.Valid {
		sUpperWarning = fmt.Sprintf("UpperWarning: %f", limitset.UpperWarning.Float64)
	} else {
		sUpperWarning = fmt.Sprintf("UpperWarning: NULL")
	}

	// and then print out the tag's Name attribute
	log.Printf("Limit is migrated! " + limitset.ID + " " + sLowerAlarm + " " + sLowerWarning + " " + sUpperAlarm + " " + sUpperWarning)
}

func newPostgresDB(metadataHost string, port int, metadataUser, metadataPass, dbname, schema string) (*DB, error) {

	psqlInfoMetadata := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable search_path=%s",
		metadataHost, port, metadataUser, metadataPass, dbname, schema)

	dbMetadata, err := sql.Open("postgres", psqlInfoMetadata)
	if err != nil {
		panic(err.Error())
	}
	return &DB{limit: dbMetadata}, nil
}

func newNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

func main() {

	dbLimitPostgresUser := "postgres"

	dbLimitPostgresHost := "localhost" // LOCALHOST
	dbLimitPostgresPassword := "postgres"
	dbLimitPostgresPort := 5432

	dbLimitsPostgres, _ := newPostgresDB(dbLimitPostgresHost, dbLimitPostgresPort, dbLimitPostgresUser, dbLimitPostgresPassword, "metadata", "limits")

	limitSets := dbLimitsPostgres.readLimits()

	//dbPostgres.printLimits(limitSets)
	dbLimitsPostgres.writeLimits(limitSets)

	dbLimitsPostgres.limit.Close()
}
