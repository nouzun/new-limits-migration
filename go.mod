module gitlab.com/nouzun/limit-migration

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.3.0
	github.com/lib/pq v1.8.0
)
